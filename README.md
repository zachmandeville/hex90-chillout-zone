# Chillout Zone for hex90

This is a small site meant as the backdrop of a chillout chat room at [hex90](https://hexninety.github.io/), a queer hacker salon in Ridgewood Queens.
Hex90 is on March 10th, 2019 at Footlight Bar in Ridgewood, from 9pm to 2am.
